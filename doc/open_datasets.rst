.. _open_datasets:

=============
Open datasets
=============

.. _holz_data0:

Higher order laue zone analysis
-------------------------------

* Zenodo DOI: `10.5281/zenodo.3476746 <https://dx.doi.org/10.5281/zenodo.3476746>`_
* Medipix3 dataset download link: https://zenodo.org/record/3476746/files/m004_LSMO_LFO_STO_medipix.hdf5?download=1
* Journal publication: `Three-dimensional subnanoscale imaging of unit cell doubling due to octahedral tilting and cation modulation in strained perovskite thin films <https://doi.org/10.1103/PhysRevMaterials.3.063605>`_
